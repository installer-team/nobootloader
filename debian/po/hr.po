# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Croatian messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
#
# Translations from iso-codes:
#   Alastair McKinstry <mckinstry@computer.org>, 2001, 2004.
#   Free Software Foundation, Inc., 2000,2004
#   Josip Rodin, 2008
#   Krunoslav Gernhard, 2004
#   Vladimir Vuksan <vuksan@veus.hr>, 2000.
#   Vlatko Kosturjak, 2001
#   Tomislav Krznar <tomislav.krznar@gmail.com>, 2012, 2013, 2014, 2015.
#   Valentin Vidic <Valentin.Vidic@CARNet.hr>, 2017
#   Milo Ivir <mail@milotype.de>, 2019, 2021.
#   gogogogi <trebelnik2@gmail.com>, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: Debian-installer 1st-stage master file HR\n"
"Report-Msgid-Bugs-To: nobootloader@packages.debian.org\n"
"POT-Creation-Date: 2019-09-26 22:04+0000\n"
"PO-Revision-Date: 2021-12-19 14:37+0000\n"
"Last-Translator: Milo Ivir <mail@milotype.de>\n"
"Language-Team: Croatian <lokalizacija@linux.hr>\n"
"Language: hr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"

#. Type: text
#. Description
#. Main menu item
#. Translators: keep below 55 columns
#. :sl1:
#: ../nobootloader.templates:1001
msgid "Continue without boot loader"
msgstr "Nastavi bez učitača pokretanja"

#. Type: error
#. Description
#. :sl4:
#: ../nobootloader.templates:2001
msgid "Failed to mount /target/proc"
msgstr "Neuspjelo montiranje /target/proc"

#. Type: error
#. Description
#. :sl4:
#: ../nobootloader.templates:2001
msgid "Mounting the proc file system on /target/proc failed."
msgstr "Montiranje proc datotečnog sustava na /target/proc nije uspjelo."

#. Type: error
#. Description
#. :sl4:
#: ../nobootloader.templates:2001
msgid "Check /var/log/syslog or see virtual console 4 for the details."
msgstr "Za pojedinosti provjerite /var/log/syslog ili četvrtu konzolu."

#. Type: error
#. Description
#. :sl4:
#: ../nobootloader.templates:2001
msgid "Warning: Your system may be unbootable!"
msgstr "Upozorenje: Vaš sustav se možda neće moći pokrenuti!"

#. Type: note
#. Description
#. :sl4:
#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:3001 ../nobootloader.templates:4001
msgid "Setting firmware variables for automatic boot"
msgstr "Postavljanje varijabli firmvera za automatsko pokretanje sustava"

#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:3001
msgid ""
"Some variables need to be set in the Genesi firmware in order for your "
"system to boot automatically.  At the end of the installation, the system "
"will reboot.  At the firmware prompt, set the following firmware variables "
"to enable auto-booting:"
msgstr ""
"Kako bi se vaš sustav automatski pokretao, neke varijable moraju biti "
"postavljene u Genesi firmveru.  Na kraju instalacije sustav će se ponovno "
"pokrenuti.  Na upitu firmvera postavite sljedeće varijable firmvera za "
"omogućavanje automatskog pokretanja sustava:"

#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:3001
msgid ""
"You will only need to do this once.  Afterwards, enter the \"boot\" command "
"or reboot the system to proceed to your newly installed system."
msgstr ""
"To ćete morati učiniti samo jednom.  Nakon toga upišite naredbu 'boot' ili "
"ponovo pokrenite sustav kako bi došli do svog svježe instaliranog sustava."

#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:3001
msgid ""
"Alternatively, you will be able to boot the kernel manually by entering, at "
"the firmware prompt:"
msgstr ""
"Alternativno, moći ćete ručno pokrenuti kernel upisivanjem na firmvare upitu:"

#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:4001
msgid ""
"Some variables need to be set in CFE in order for your system to boot "
"automatically. At the end of installation, the system will reboot. At the "
"firmware prompt, set the following variables to simplify booting:"
msgstr ""
"Kako bi se vaš sustav automatski pokretao, neke varijable moraju biti "
"postavljene u CFE-u. Na kraju instalacije sustav će se ponovo pokrenuti. Na "
"upitu firmvera postavite sljedeće varijable firmvara kako bi pojednostavili "
"pokretanje sustava:"

#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:4001
msgid ""
"You will only need to do this once. This enables you to just issue the "
"command \"boot_debian\" at the CFE prompt."
msgstr ""
"Ovo ćete morati napraviti samo jednom. To će vam omogućiti da na CFE upitu "
"upisujete samo naredbu \"boot_debian\"."

#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:4001
msgid ""
"If you prefer to auto-boot on every startup, you can set the following "
"variable in addition to the ones above:"
msgstr ""
"Ako želite automatsko pokretanje svaki puta, možete postaviti sljedeću "
"varijablu, uz one gore navedene:"

#. Type: note
#. Description
#. :sl3:
#: ../nobootloader.templates:5001
msgid "No boot loader installed"
msgstr "Učitač pokretanja nije instaliran"

#. Type: note
#. Description
#. :sl3:
#: ../nobootloader.templates:5001
msgid ""
"No boot loader has been installed, either because you chose not to or "
"because your specific architecture doesn't support a boot loader yet."
msgstr ""
"Učitač pokretanja nije instaliran, ili zato što ste tako odabrali, ili zato "
"što vaša određena arhitektura još ne podržava učitača pokretanja."

#. Type: note
#. Description
#. :sl3:
#: ../nobootloader.templates:5001
msgid ""
"You will need to boot manually with the ${KERNEL} kernel on partition "
"${BOOT} and ${ROOT} passed as a kernel argument."
msgstr ""
"Morat ćete ručno pokrenuti sustav s kernelom ${KERNEL} na particiji ${BOOT}, "
"te argumentom ${ROOT} proslijeđenim kernelu."
